#!/usr/bin/python
def merge_sort(alist, anotherlist):
	if len(alist) <= 1:
		return (alist, anotherlist)
	middle = len(alist) // 2
	left = alist[:middle]
	right = alist[middle:]
	left2 = anotherlist[:middle]
	right2 = anotherlist[middle:]
	(left, left2) = merge_sort(left, left2)
	(right, right2) = merge_sort(right, right2)
	return merge(left, left2, right, right2)

def merge(left, left2, right, right2):
	result = []
	result2 = []
	left_idx, right_idx = 0, 0
	while left_idx < len(left) and right_idx < len(right):
		if left[left_idx] <= right[right_idx]:
			result.append(left[left_idx])
			result2.append(left2[left_idx])
			left_idx += 1
		else:
			result.append(right[right_idx])
			result2.append(right2[right_idx])
			right_idx += 1
	if left:
		result.extend(left[left_idx:])
		result2.extend(left2[left_idx:])
	if right:
		result.extend(right[right_idx:])
		result2.extend(right2[right_idx:])
	return (result, result2)

alist = [54,26,93,17,77,31,44,55,20]
anotherlist = [1,2,3,4,5,6,7,8,9]
left = [1, 3, 5]
right = [2, 4, 6]
(alist, anotherlist) = merge_sort(alist, anotherlist)


#def sort_by_size(objects):
#	all_sizes = []
#	for i in range(0, len(objects)):
#		cur_object = objects[i]
#		cur_size = cur_object.size
#		all_sizes.append(cur_size)
