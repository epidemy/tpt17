import numpy as np
import cv2

def getFaceAmountInShot(shot):
    
    face_cascade = cv2.CascadeClassifier('usr/share/opencv/haarcascades/haarcascade_frontalface_default.xml')
    amount = 0
    counts = []
    
    for frame in shot:
        counts.append(getFaceAmountInFrame(frame, face_cascade))
    
    counts = np.bincount(counts)
    return np.argmax(counts)
    
def getFaceAmountInFrame(frame, face_cascade):
    
    amount = 0
    detect(frame)
    frame_g = cv2.cvtColor(frame, cv2.COLOR_BGR2GRAY)
    faces = face_cascade.detectMultiScale(
        frame_g,
        scaleFactor=1.1,
        minNeighbors=5,
        minSize=(50, 30),
        flags = cv2.cv.CV_HAAR_SCALE_IMAGE
    )
    for (x,y,w,h) in faces:
        print 'ello'
    amount = len(faces)
    print amount
    return amount
    
def detect(frame):
    face_cascade = cv2.CascadeClassifier('/usr/share/opencv/haarcascades/haarcascade_frontalface_alt.xml')
    #face_profile = cv2.CascadeClassifier('/usr/share/opencv/haarcascades/haarcascade_profileface.xml')
    face_mouth = cv2.CascadeClassifier('/usr/share/opencv/haarcascades/haarcascade_mcs_mouth.xml')
    gray = cv2.cvtColor(frame, cv2.COLOR_BGR2GRAY)
    
    faces = face_cascade.detectMultiScale(gray, 1.3, 2, cv2.cv.CV_HAAR_SCALE_IMAGE, (0,0))    
    print "# of faces: " + str(len(faces))

    for (x,y,w,h) in faces:
        cv2.rectangle(frame,(x,y),(x+w,y+h),(255,0,0),2)
        roi_gray = gray[y:y+h, x:x+w]
        #roi_color = frame[y:y+h, x:x+w]
        mouth= face_mouth.detectMultiScale(roi_gray, 1.4, 8,cv2.cv.CV_HAAR_DO_CANNY_PRUNING)
        
        #roi_gray_copy = deepcopy(roi_gray)

        mouths = 0
        for (ex,ey,ew,eh) in mouth:
        
            if ey < 0.5*h:
                continue

            mouths += 1

        print "# of mounths: " + str(mouths) 

if __name__ == '__main__':
    shot = []
    img = cv2.imread('group.jpg')
    shot.append(img)
    amount = getFaceAmountInShot(shot)
    print amount
    #print(getFaceAmountInShot(shot))