#!/usr/bin/python
import math
import cv2
import numpy as np
from sklearn.cluster import KMeans

#must be uneven
overlap = 256

def convert_histogram(histogram):
	converted0 = []
	tot_amount = 0
	for i in range (0, len(histogram)):
		cur_value = (histogram[i])[0]
		converted0.append(cur_value)
		tot_amount += cur_value
	for i in range (0, len(converted0)):
		cur_value = converted0[i]
		converted0[i] = cur_value/tot_amount
	#converted1 = []
	#for i in range (0, len(converted0)):
	#	average = 0
	#	amount = 0
	#	for j in range (i-(overlap-1)/2, i+(overlap-1)/2+1):
	#		if (j >= 0 and j < len(converted0)):
	#			average += converted0[j]
	#			amount += 1
	#	average /= amount
	#	converted1.append(average)
	#print(converted0)
	return converted0

def smart_append(list1, list2):
	long_list = []
	for i in range(0, len(list1)):
		long_list.append(list1[i])
	for i in range(0, len(list2)):
		long_list.append(list2[i])
	return long_list

ocean = ["ocean0.jpg", "ocean1.jpg", "ocean2.jpg", "ocean3.jpg", "ocean4.jpg", "ocean5.jpg", "ocean6.jpg", "ocean7.jpg", "ocean8.jpg", "ocean9.jpg"]
jungle = ["jungle0.jpg", "jungle1.jpg", "jungle2.jpg", "jungle3.jpg", "jungle4.jpg", "jungle5.jpg", "jungle6.jpg", "jungle7.jpg", "jungle8.jpg", "jungle9.jpg"]

ocean_histograms = []
jungle_histograms = []

for i in range (0, len(ocean)):
	concat = "ocean" + str(i) + ".jpg"
	img = cv2.imread(concat)
	hsv = cv2.cvtColor(img,cv2.COLOR_BGR2HSV)
	hist0 = cv2.calcHist([img],[0],None,[256],[0,256])
	hist1 = cv2.calcHist([img],[1],None,[256],[0,256])
	hist2 = cv2.calcHist([img],[2],None,[256],[0,256])
	hist = smart_append(hist0, hist1)
	hist = smart_append(hist, hist2)
	#ocean_histograms.append(convert_histogram(hist))
	hist9 = cv2.calcHist([img],[0],None,[256],[0,256])
	hist9.flatten()
	ocean_histograms.append(hist9)
for i in range (0, len(jungle)):
        concat = "jungle" + str(i) + ".jpg"
        img = cv2.imread(concat)
	hsv = cv2.cvtColor(img,cv2.COLOR_BGR2HSV)
	hist0 = cv2.calcHist([img],[0],None,[256],[0,256])
	hist1 = cv2.calcHist([img],[1],None,[256],[0,256])
	hist2 = cv2.calcHist([img],[2],None,[256],[0,256])
	hist = smart_append(hist0, hist1)
	hist = smart_append(hist, hist2)
        #jungle_histograms.append(convert_histogram(hist))
	hist9 = cv2.calcHist([img],[0],None,[256],[0,256])
	hist9.flatten()
	jungle_histograms.append(hist9)

all_histograms = []

#for i in range (0, len(ocean_histograms)):
#	all_histograms.append(ocean_histograms[i])
#for i in range (0, len(jungle_histograms)):
	#result = cv2.compareHist(jungle_histograms[i], ocean_histograms[i], cv2.cv.CV_COMP_CORREL)
	#print(result)
	#if i > 0:
	#	print(cv2.compareHist(jungle_histograms[i], jungle_histograms[i-1], cv2.cv.CV_COMP_CORREL))
	#all_histograms.append(jungle_histograms[i])

#maxi = 0
#for i in range (0, len(hist)):
#	if(hist[i] > maxi):
#		maxi = hist[i]

#est = KMeans(n_clusters=2)
#result = est.fit_predict(all_histograms)#, [0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1])
#print(result)

#for i in range (0, int(math.floor(len(ocean_histograms)/2))):
#	cur_histogram = ocean_histograms[i]
#	est.fit(cur_histogram, 0)
#for i in range (0, int(math.floor(len(jungle_histograms)/2))):
#	cur_histogram = jungle_histograms[i]
#	est.fit(cur_histogram, 1)
#for i in range (0, len(ocean_histograms)):
#	cur_histogram = ocean_histograms[i]
#	pred = est.predict(cur_histogram)
#for i in range (0, len(jungle_histograms)):
#	cur_histogram = jungle_histograms[i]
#	pred = est.predict(cur_histogram)

def mergeSort(alist):
    print("Splitting ",alist)
    if len(alist)>1:
        mid = len(alist)//2
        lefthalf = alist[:mid]
        righthalf = alist[mid:]

        mergeSort(lefthalf)
        mergeSort(righthalf)

        i=0
        j=0
        k=0
        while i<len(lefthalf) and j<len(righthalf):
            if lefthalf[i]<righthalf[j]:
                alist[k]=lefthalf[i]
                i=i+1
            else:
                alist[k]=righthalf[j]
                j=j+1
            k=k+1

        while i<len(lefthalf):
            alist[k]=lefthalf[i]
            i=i+1
            k=k+1

        while j<len(righthalf):
            alist[k]=righthalf[j]
            j=j+1
            k=k+1
    print("Merging ",alist)

alist = [54,26,93,17,77,31,44,55,20]
mergeSort(alist)
print(alist)


#def sort_by_size(objects):
#	all_sizes = []
#	for i in range(0, len(objects)):
#		cur_object = objects[i]
#		cur_size = cur_object.size
#		all_sizes.append(cur_size)
