# -*- coding: utf-8 -*-
"""
Created on Thu Mar 19 09:29:56 2015

@author: ext7216
"""
import xml.etree.ElementTree as ET
import os
from os import listdir
from os.path import isfile, join
import cv2
from pprint import pprint
import itertools
from sklearn.cluster import KMeans


def get_shot_times(filename):
    tree = ET.parse(filename)
    root = tree.getroot()
    
    times=[]
    for node in root.find("Episode").find("Section"):
        times.append({"startTime": node.attrib["startTime"], "endTime": node.attrib["endTime"]})
    return times


def drop_frames_to_disk(filename, startTime, endTime):
    step = "1"
    frames = "1"
    delayedStartTime = str(float(startTime)+1)
    outputdir = get_or_create_dir(filename, startTime, endTime)
    cmd = "mplayer -vo jpeg:outdir={} -ss {} -endpos {} -sstep {} -frames {} {}".format(
        outputdir, delayedStartTime, endTime, step, frames, full_filename(filename))
    os.system(cmd)


def full_filename(filename):
    return "/srv/athens/ces_datascience/videos/" + filename


def get_dir_name(filename, startTime, endTime):
    return "frames/{}/{}-{}/".format(filename, startTime, endTime)


def get_or_create_dir(filename, startTime, endTime):
    directory = get_dir_name(filename, startTime, endTime)
    if not os.path.exists(directory):
        os.makedirs(directory)
    return directory
    
    
def read_images(filename, startTime, endTime):
    directory = get_dir_name(filename, startTime, endTime)
    files = [f for f in listdir(directory) if isfile(join(directory,f))]
    return [cv2.imread(directory + f) for f in files]
    
    
def read_shots(filename, shot_times):
    return [read_images(filename, shot["startTime"], shot["endTime"]) for shot in shot_times]
    
    
def video_to_frames(video_name, shot_times):
    for shot in shot_times:
        drop_frames_to_disk(video_name, shot["startTime"], shot["endTime"])\
        

def smart_append(list1, list2):
	long_list = []
	for i in range(0, len(list1)):
		long_list.append(list1[i])
	for i in range(0, len(list2)):
		long_list.append(list2[i])
	return long_list
 
def calculate_histograms(images):
    all_histograms = []
    for img in images:
        hist0 = cv2.calcHist(img,[0],None,[256],[0,256])
        hist1 = cv2.calcHist(img,[1],None,[256],[0,256])
        hist2 = cv2.calcHist(img,[2],None,[256],[0,256])
        hist = smart_append(hist0, hist1)
        hist = smart_append(hist, hist2)
        all_histograms.append(convert_histogram(hist))
    return all_histograms
        


def compare_shots(all_histograms):
    comparisons = []
    for i in range (0, len(all_histograms)):
        cur_comparison = []
        for j in range (0, i):
            first_histogram = all_histograms[i]
            second_histogram = all_histograms[j]
            result = cv2.compareHist(first_histogram, second_histogram, cv2.cv.CV_COMP_CORREL)
            cur_comparison.append(result)
        comparisons.append(cur_comparison)
    return comparisons

def convert_histogram(histogram):
    overlap = 10
    converted0 = []
    tot_amount = 0
    for i in range (0, len(histogram)):
        cur_value = (histogram[i])[0]
        converted0.append(cur_value)
        tot_amount += cur_value
    for i in range (0, len(converted0)):
        cur_value = converted0[i]
        converted0[i] = cur_value/tot_amount
        converted1 = []
    for i in range (0, len(converted0)):
        average = 0
        amount = 0
        for j in range (i-(overlap-1)/2, i+(overlap-1)/2+1):
            if (j >= 0 and j < len(converted0)):
                average += converted0[j]
                amount += 1
        average /= amount
        converted1.append(average)
    return converted1

def cluster(histograms):
    est = KMeans(n_clusters=6)
    result = est.fit_predict(histograms)
    print result
    
def findKeyPoints(img, template, distance=200):
	detector = cv2.FeatureDetector_create("SIFT")
	descriptor = cv2.DescriptorExtractor_create("SIFT")

	skp = detector.detect(img)
	skp, sd = descriptor.compute(img, skp)

	tkp = detector.detect(template)
	tkp, td = descriptor.compute(template, tkp)

	flann_params = dict(algorithm=1, trees=4)
	flann = cv2.flann_Index(sd, flann_params)
	idx, dist = flann.knnSearch(td, 1, params={})
	del flann

	dist = dist[:,0]/2500.0
	dist = dist.reshape(-1,).tolist()
	idx = idx.reshape(-1).tolist()
	indices = range(len(dist))
	indices.sort(key=lambda i: dist[i])
	dist = [dist[i] for i in indices]
	idx = [idx[i] for i in indices]
	skp_final = []
	for i, dis in itertools.izip(idx, dist):
         if dis < distance:
             skp_final.append(skp[i])

	flann = cv2.flann_Index(td, flann_params)
	idx, dist = flann.knnSearch(sd, 1, params={})
	del flann

	dist = dist[:,0]/2500.0
	dist = dist.reshape(-1,).tolist()
	idx = idx.reshape(-1).tolist()
	indices = range(len(dist))
	indices.sort(key=lambda i: dist[i])
	dist = [dist[i] for i in indices]
	idx = [idx[i] for i in indices]
	tkp_final = []
	for i, dis in itertools.izip(idx, dist):
         if dis < distance:
            tkp_final.append(tkp[i])
	return skp_final, tkp_final
    
def calculate_gradiants(images, factor):
      all_descriptors = []
      for image in images:
          img = cv2.imread(image)
          img = cv2.cvtColor(img, cv2.COLOR_BGR2GRAY)
          hog = cv2.HOGDescriptor()
          newx,newy = img.shape[1]/5,img.shape[0]/5 #new size (w,h)
          newimage = cv2.resize(img,(newx,newy))
          h = hog.compute(newimage, hog.blockStride,hog.cellSize)
          cur_descriptor = []
          for element in h:
              cur_descriptor.append(element[0])
          total = 0
          for element in cur_descriptor:
              total += element
          for i in range (0, len(cur_descriptor)):
              cur_descriptor[i] = cur_descriptor[i]/total*factor
          all_descriptors.append(cur_descriptor)
      return all_descriptors
      
def merge_histograms_and_gradiants(histograms, descriptors):
    all_merged = []
    for i in range (0, len(histograms)):
        histogram = histograms[i]
        descriptor = descriptors[i]
        merged = smart_append(histogram, descriptor)
        all_merged.append(merged)
    return all_merged

if __name__ == "__main__":
    video_name = "06-11-22.avi"
    #trs_name = "06-11-22.trs"
    #shot_times = get_shot_times(trs_name)
    #video_to_frames(video_name, shot_times)
    #shots = read_shots(video_name, shot_times)
    #comparisons = compare_shots(calculate_histograms(shots))
    #histograms = calculate_histograms(shots)
    #radiances = calculate_gradiants(shots, 1)
    #merged = merge_histograms_and_gradiants(histograms, radiances)
    #cluster(merged)
    