import cv2
import numpy as np

img = cv2.imread('ocean1.jpg')
gray= cv2.cvtColor(img,cv2.COLOR_BGR2GRAY)

sift = cv2.SIFT()
#kp = sift.detect(gray,None)
kp,des = sift.detectAndCompute(gray,None)
print(len(des))

#img=cv2.drawKeypoints(gray,kp,flags=cv2.DRAW_MATCHES_FLAGS_DRAW_RICH_KEYPOINTS)

#cv2.imwrite('sift_ocean0.jpg',img)
