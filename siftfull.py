import cv2
import cv2.cv as cv
import numpy as np
import itertools
import sys
from PIL import Image

def findKeyPoints(img, template, distance=200):
    detector = cv2.FeatureDetector_create("SIFT")
    descriptor = cv2.DescriptorExtractor_create("SIFT")

    skp = detector.detect(img)
    skp, sd = descriptor.compute(img, skp)

    tkp = detector.detect(template)
    tkp, td = descriptor.compute(template, tkp)

    flann_params = dict(algorithm=1, trees=4)
    flann = cv2.flann_Index(sd, flann_params)
    idx, dist = flann.knnSearch(td, 1, params={})
    del flann

    dist = dist[:,0]/2500.0
    dist = dist.reshape(-1,).tolist()
    idx = idx.reshape(-1).tolist()
    indices = range(len(dist))
    indices.sort(key=lambda i: dist[i])
    dist = [dist[i] for i in indices]
    idx = [idx[i] for i in indices]
    skp_final = []
    for i, dis in itertools.izip(idx, dist):
        if dis < distance:
            skp_final.append(skp[i])

    flann = cv2.flann_Index(td, flann_params)
    idx, dist = flann.knnSearch(sd, 1, params={})
    del flann

    dist = dist[:,0]/2500.0
    dist = dist.reshape(-1,).tolist()
    idx = idx.reshape(-1).tolist()
    indices = range(len(dist))
    indices.sort(key=lambda i: dist[i])
    dist = [dist[i] for i in indices]
    idx = [idx[i] for i in indices]
    tkp_final = []
    for i, dis in itertools.izip(idx, dist):
        if dis < distance:
            tkp_final.append(tkp[i])
    #print idx[0:100]
    #print dist[0:100]
    #print sum(dist[0:50])/50
    #print 'mklsjdfklsfjsdlfjk'
    #print tkp
    return skp_final, tkp_final

def drawKeyPoints(img, template, skp, tkp, num=-1):
    h1, w1 = img.shape[:2]
    h2, w2 = template.shape[:2]
    nWidth = w1+w2
    nHeight = max(h1, h2)
    hdif = (h1-h2)/2
    newimg = np.zeros((nHeight, nWidth, 3), np.uint8)
    newimg[hdif:hdif+h2, :w2] = template
    newimg[:h1, w2:w1+w2] = img

    maxlen = min(len(skp), len(tkp))
    if num < 0 or num > maxlen:
        num = maxlen
    for i in range(num):
        pt_a = (int(tkp[i].pt[0]), int(tkp[i].pt[1]+hdif))
        pt_b = (int(skp[i].pt[0]+w2), int(skp[i].pt[1]))
        cv2.line(newimg, pt_a, pt_b, (255, 0, 0))
    return newimg


def match():
    img = cv2.imread('6.jpg')
    hog = cv2.HOGDescriptor()
    newx,newy = img.shape[1]/5,img.shape[0]/5 #new size (w,h)
    newimage = cv2.resize(img,(newx,newy))
    h = hog.compute(newimage, hog.blockStride,hog.cellSize)
    print h 
    print h.size
    img = img[:440,:]
    temp = cv2.imread('2.jpg')
    temp = temp[:440,:]
    try:
    	dist = int(sys.argv[3])
    except IndexError:
    	dist = 200
    try:
    	num = int(sys.argv[4])
    except IndexError:
      num = -1
    skp, tkp = findKeyPoints(img, temp, dist)
    #for i in range (0,20):
        #print skp[i].pt
        #print skp[i].response   
        #print skp[i].size 
        #print skp[i].angle 
        #print skp[i].octave 
    
    #newimg = drawKeyPoints(img, temp, skp, tkp, num)
    #printme = Image.fromarray(newimg)
    #cv.ShowImage( "My Photo Window",printme)
    #cv2.imshow("image", newimg)
    #cv2.waitKey(0)
    
match()
